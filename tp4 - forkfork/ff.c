#include <stdio.h>
#include <stdlib.h>

#include "forkfork.h"

void f()   
{
    printf("forkfork réussi !\n");
}

int main(int argc, char const *argv[])
{
    /* code */

    forkfork(f, "toto");
    return 0;
}