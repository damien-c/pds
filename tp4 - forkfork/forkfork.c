#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "forkfork.h"

void forkfork(func_t f, void *arg)
{
     pid_t pid1;
     pid_t pid2;
     int status;

     if ((pid1 = fork())) {
             /* parent process A */
             waitpid(pid1, &status, 0);
     } else if (!pid1) {
             /* child process B */
             if ((pid2 = fork())) {
                     exit(0);
             } else if (!pid2) {
                     /* child process C */
                     f(arg);
             } else {
                     /* error */
                     perror("erreur fork du petit fils");
                     exit(EXIT_FAILURE);
             }
     } else {
             /* error */
             perror("erreur fork");
             exit(EXIT_FAILURE);
     }
}

