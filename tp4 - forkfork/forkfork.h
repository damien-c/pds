#ifndef FORKFORK_H
#define FORKFORK_H

typedef void (*func_t) (void *);

extern void forkfork(func_t, void *);

#endif
