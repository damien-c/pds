#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


/*pid_t ifork() 
{
	fprintf(stderr, "fork() %d ? (^abort) ", getpid());
	fflush(stderr);
	getchar();
	return fork();
}*/

int main(void)
{
    pid_t pid;
    pid_t pid2;
    int i, j, status;

    for (i = 1; i < 11; i++)
    {
    	pid = fork();
    	if(!pid)
    	{
    		for (j = 0; j < 100000000; j++) ;
    		printf("Je suis le process %d n° %d\n", getpid(), i);
    		for (j = 0; j < 100000000; j++) ;
    		exit(i);
    	}
    }
    for (i = 1; i < 11; i++)
    {
    	pid2 = wait(&status);
    	printf("Arrivé : %d : process n° %d == %d\n", i, (status/256), pid2);
    }
 
    return  0;
}