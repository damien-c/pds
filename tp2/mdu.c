/* Tristan Lippens et Damien Cornette - L3S6 - G5 - PDS - TP "Du" */

#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdio.h>   
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h> 
#include <sys/types.h>

/* Caractère utilisé pour différencier une commande */
#define CHAR_CMD '-'

static int opt_follow_links = 0;
static int opt_apparent_size = 0;

/* Transforme un caractère en commande */
void char2cmd(char c)
{
	switch(c)
	{
		case 'b': opt_apparent_size = 1; break;
		case 'L': opt_follow_links = 1; break;
		default	: printf("Unknown command '%c'\n",c);
	}

}

/* Transforme un argument en commande(s) */
void getcmd(char *arg)
{
	size_t arglen = strlen(arg);
	
	while((arglen--)>1) /* Pour chaque caractère de l'argument */
		char2cmd(arg[arglen]); /* Sauvegarder les commandes trouvées */
}

/* Parse les arguments un  à un */
void parse(int argc,char *argv[])
{	
	while(argc--) /* Pour tous les arguments */
		switch(argv[argc][0]) /* En fonction du premier caractère de l'argument */
		{
			/* Transformer l'argument qui commence par CHAR_CMD en commandes */
			case CHAR_CMD: getcmd(argv[argc]); 
		}
}

/* Test si le nom est valide, i.e ce n'est pas le dossier courant ni le précédent */
int valid_name(const char *pathname)
{
	return (strcmp(pathname,"..") && strcmp(pathname,"."));
}

/* Réimplémentation de du */
int du_file(const char *pathname)
{
	struct stat st;
	int status;
	
	/* Récupérer les informations de pathname */
	status = lstat(pathname,&st);

	/* Si il y a une erreur */
	if(status!=0)
		perror(pathname); /* Afficher l'erreur */
	else if(S_ISREG(st.st_mode) || S_ISLNK(st.st_mode)) /* Sinon si c'est un fichier ou un lien */
	{
		int size;
		/* Choisir la taille à afficher en fonction des options choisies */
		size = opt_apparent_size ? st.st_size : st.st_blocks; 
		
		/* Si c'est un lien et qu'il faut le suivre */
		if(S_ISLNK(st.st_mode) && opt_follow_links)
		{
			char link_path[PATH_MAX+1];
			/* Lire le lien */
			int status = readlink(pathname,link_path,PATH_MAX);
			
			if(status > 0)
			{
				link_path[status] = '\0'; /* Terminer la string */
				size += du_file(link_path); /* Appel récursif à du_file pour compter tous les fichiers */
			}
			else perror(link_path);
		}
		
		return size; /* Retourner la taille totale */
	}
	else if(S_ISDIR(st.st_mode)) /* Sinon si c'est un dossier */
	{
		DIR *dirp;
		struct dirent *dp;
		char entry_path[PATH_MAX+1];
		int size;
		
		/* Choisir la taille à afficher en fonction des options choisies */
		size = opt_apparent_size ? st.st_size : st.st_blocks;
		/* Ouvrir le dossier */
		dirp = opendir(pathname);
		
		/* Si l'ouverture a échoué */
		if(!dirp)
			perror(pathname); /* Afficher l'erreur */
		else while((dp=readdir(dirp))) /* Sinon pour tous les fichiers du dossier */
		{			
			if(valid_name(dp->d_name)) /* Si le nom est valide */
			{
				/* Concatener les noms */
				snprintf(entry_path,PATH_MAX,"%s/%s",pathname,dp->d_name);
				size += du_file(entry_path); /* Appel récursif à du_file pour compter tous les fichiers */
			}
		}
		
		closedir(dirp); /* Fermer le dossier ouvert pour éviter les problèmes ! */
		return size; /* Retourner la taille totale */
	}
	
	/* L'entrée n'est pas connue, on la 'saute' */
	fprintf(stderr,"Entrée %s de type ignoré\n",pathname);
	return 0;	
}

int main(int argc,char **argv)
{
	/* Parser tous les arguments pour récupérer les commandes */
	parse(argc,argv);
	
	if(argc>1)
		while(--argc>0)
			if(argv[argc][0]!=CHAR_CMD)
				printf("%i %s\n",du_file(argv[argc]),argv[argc]);
	return 0;
}
