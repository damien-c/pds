#!/bin/sh
# Test sur un dossier qui contient des fichiers non accessibles (permissions)
./mdu /etc/
du -B 512 -s /etc/
# Test sur un fichier ou dossier qui n'existe pas
./mdu foo
du foo
# Test sur un lien symbolique avec option activée
ls -al /var/www/openwrt/trunk/
./mdu -L /var/www/openwrt/trunk/
du -B 512 -s -L /var/www/openwrt/trunk/
# Test sur un lien symbolique avec option désactivée
./mdu /var/www/openwrt/trunk/
du -B 512 -s /var/www/openwrt/trunk/
# Test sur un fichier non géré
./mdu /dev/sda
# Test sur le dossier courant et précédent
./mdu ./
du -B 512 -s ./
./mdu ../
du -B 512 -s ../

