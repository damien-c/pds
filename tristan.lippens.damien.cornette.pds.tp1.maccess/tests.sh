#!/bin/bash
folder_len=$1
file_len=$2
nb=$3
folder_string=""
file_string=""
path=""

while [ $folder_len -gt 0 ]
do
	folder_string="$folder_string""a"
	let $[folder_len-=1]
done

while [ $file_len -gt 0 ]
do
	file_string="$file_string""b"
	let $[file_len-=1]	
done

while [ $nb -gt 0 ]
do
	path="$path/$folder_string"
	let $[nb-=1]
done

path="$path/$file_string"

echo $path
