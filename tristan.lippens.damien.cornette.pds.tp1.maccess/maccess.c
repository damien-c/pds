/* Tristan Lippens et Damien Cornette - L3S6 - G5 - PDS - TP "Access" */
#include <limits.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* Caractère utilisé pour différencier une commande */
#define CHAR_CMD '-'
/* Représente le mode verbeux, doit être une puissance de 2 supérieure à 4 */
#define VERBOSE 8
/* Test si le mode verbeux est activé */
#define IS_VERBOSE(x) (x&VERBOSE)
/* Récupère les modes de fichier depuis toutes les commandes */
#define FILE_CMD(x) (IS_VERBOSE(x)?x^VERBOSE:x)

void prlimit()
{
	printf("NAME_MAX = %i\nPATH_MAX = %i\n",NAME_MAX,PATH_MAX);
}

/* Transforme un caractère en commande */
int char2cmd(char c)
{
	switch(c)
	{
		case 'r': return R_OK;
		case 'w': return W_OK;
		case 'x': return X_OK;
		case 'v': return VERBOSE;
		case 'p': prlimit(); break;
		default	: printf("Unknown command '%c'\n",c);
	}

	return 0;
}

/* Transforme un argument en commande(s) */
void getcmd(char *arg, int *cmd)
{
	size_t arglen = strlen(arg);
	
	while((arglen--)>1) /* Pour chaque caractère de l'argument */
		*cmd|=char2cmd(arg[arglen]); /* Sauvegarder les commandes trouvées */
}

/* Parse les arguments un  à un */
void parse(int argc,char *argv[],int *cmd)
{	
	while(argc--) /* Pour tous les arguments */
		switch(argv[argc][0]) /* En fonction du premier caractère de l'argument */
		{
			/* Transformer l'argument qui commence par CHAR_CMD en commandes */
			case CHAR_CMD: getcmd(argv[argc],cmd); 
		}
}

/* Appelle access */
int maccess(int argc,char *argv[],int cmd)
{
	while((--argc)>1) /* Pour tous les arguments */
		if(argv[argc][0]!=CHAR_CMD) /* Si l'argument n'est pas une commande */
		{
			int err=access(argv[argc],FILE_CMD(cmd)); /* Appeler access avec un supposé fichier/dossier */
			
			if(IS_VERBOSE(cmd)) /* Si le mode verbeux est activé */
			{				
				if(err==-1) /* Si il y a eu une erreur */
					perror(argv[argc]); /* Afficher l'erreur rencontrée */
				else
					printf("%s: Success\n",argv[argc]); /* Sinon afficher que tout s'est bien passé */
			}
		}
	
	return errno>0;
}

int main(int argc,char *argv[])
{
	int cmd=F_OK;
	parse(argc,argv,&cmd); /* Parser tous les arguments pour récupérer les commandes */
	return maccess(argc,argv,cmd); /* Appeler access avec les commandes récupérées */
}
