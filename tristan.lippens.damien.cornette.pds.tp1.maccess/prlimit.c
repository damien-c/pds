/* Tristan Lippens et Damien Cornette - L3S6 - G5 - PDS - TP "Access" */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("NAME_MAX = %i\nPATH_MAX = %i\n",NAME_MAX,PATH_MAX);
	
	return EXIT_SUCCESS;
}
