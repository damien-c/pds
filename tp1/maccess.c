#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>


int maccess(const char *pathname, int mode, int verbose) {
	printf("file : %s\nmode : %d\nverbose : %d\n\n", pathname, mode, verbose);
	return 0;
}


int main (int argc, char *argv[]) {
	/*printf("F_OK : %d\n", F_OK);
	printf("R_OK : %d\n", R_OK);
	printf("W_OK : %d\n", W_OK);
	printf("X_OK : %d\n", X_OK);
	printf("tout : %d\n", F_OK|R_OK|W_OK|X_OK);*/

	int mode,i,verbose;
	char* file = malloc(1000);
	mode=0;
	verbose=0;

	for(i=1; i<argc; i++) {
		if(argv[i][0] == '-') {
			switch(argv[i][1]) {
				case 'r' : 
					mode = mode | R_OK;
					break;
				case 'w' : 
					mode = mode | W_OK;
					break;
				case 'x' : 
					mode = mode | X_OK;
					break;
				case 'v' : 
					verbose=1;
					break;
				default : 
					break;
								
			}
		}
		else {
			strcpy(file, argv[i]);
		}
	}

	maccess(file,mode,verbose);
	
	return EXIT_SUCCESS;
}
